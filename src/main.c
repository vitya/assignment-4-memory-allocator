#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <inttypes.h>

static void* show_heap_init(size_t size) {
  printf("\n>> heap_init(%zu)\n", size);
  void *heap = heap_init(size);
  debug_heap(stdout, heap);
  puts("\n");
  return heap;
}

// TODO: how to use PRIu64? Compiler throws errors...
static void* show_malloc(size_t size, void* heap) {
  printf("\n>> malloc(%zu)\n", size);
  void* mem_ptr = _malloc(size);
  assert(mem_ptr != NULL);
  debug_heap(stdout, heap);
  return mem_ptr;
}

static void show_free(void* ptr, void* heap) {
  printf("\n>> free(%p)\n", ptr);
  _free(ptr);
  debug_heap(stdout, heap); 
}

static void test_default_memory_allocation(void* heap) {
  puts("--- Test: Default memory allocation ---");
  show_malloc(100, heap);
  show_malloc(100, heap);
  show_malloc(100000, heap);
  puts("\n\n");
}

static void test_allocate_and_free(void* heap) {
  puts("--- Test: Allocate and free ---\n");
  void* ptr = show_malloc(3000, heap);
  show_free(ptr, heap);
  puts("\n\n");
}

static void test_allocate_and_free_multiple(void* heap) {
  puts("--- Test: Allocate and free multiple ---\n");
  void* a = show_malloc(3000, heap);
  show_malloc(100, heap);
  show_malloc(700, heap);
  void* b = show_malloc(4000, heap);
  show_free(a, heap);
  show_free(b, heap);
  puts("\n\n");
}

static void test_new_region_extends(void* heap) {
  puts("--- Test: New region extends ---\n");
  show_malloc(10000, heap);
  puts("\n\n");
}

static void test_new_region_does_not_extend(void* heap) {
  puts("--- Test: New region does not extend ---\n");
  uint8_t* ptr = show_malloc(6000, heap);
  show_free(ptr, heap);
  mmap_continuing_region_if_possible(ptr + 10000, 100);
  show_malloc(9000, heap);
  puts("\n\n");
}

int main() {
  void* heap = show_heap_init(100);
  test_default_memory_allocation(heap);
  test_allocate_and_free(heap);
  test_allocate_and_free_multiple(heap);
  test_new_region_extends(heap);
  test_new_region_does_not_extend(heap);
}
